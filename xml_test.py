import urllib2
import xmltodict
import base64
import os
import subprocess

def homepage(username, password, url):
    request = urllib2.Request(URL)
    base64string = base64.b64encode('%s:%s' % (username, password))
    request.add_header("Authorization", "Basic %s" % base64string)   
    result = urllib2.urlopen(request)
    data = result.read()
    result.close()
    #convert xml to dict
    data = xmltodict.parse(data)
    
    #debug print pass or fail count
    #print data['testResult']['failCount']
    #print data['testResult']['passCount']
    
    #check if the environment variables exist
    command = "cat ~/.bashrc | grep -q JENKINS_FAIL_COUNT ; echo $?"
    returnCode = subprocess.check_output(command, shell=True)
    if int(returnCode) == 1:
        #if it doesn't exist create it
        command = "echo " + "'" + "export JENKINS_FAIL_COUNT=" + '"' + str(0) + '"' + "'" + " >> ~/.bashrc"
        os.system(command)
    command = "cat ~/.bashrc | grep -q JENKINS_PASS_COUNT ; echo $?"
    returnCode = subprocess.check_output(command, shell=True)
    if int(returnCode) == 1:
        command = "echo " + "'" + "export JENKINS_PASS_COUNT=" + '"' + str(0) + '"' + "'" + " >> ~/.bashrc"
        os.system(command)
    
    #modify the environment variables with sed
    command = "sed -i " + "'" + "s/^export JENKINS_FAIL_COUNT=.*$/export JENKINS_FAIL_COUNT=" + '"' + str(data['testResult']['failCount']) + '"' + "/" + "'" + " ~/.bashrc"
    os.system(command)
    command = "sed -i " + "'" + "s/^export JENKINS_PASS_COUNT=.*$/export JENKINS_PASS_COUNT=" + '"' + str(data['testResult']['passCount']) + '"' + "/" + "'" + " ~/.bashrc"
    os.system(command)


if __name__ == '__main__':
    USERNAME = 'username goes here'
    PASSWORD = 'password goes here'
    URL = 'http://jenkins.windhoverlabs.lan/job/Airliner/job/Softsim/job/Current-Sprint/lastCompletedBuild/testReport/api/xml'
    homepage(USERNAME, PASSWORD, URL)
