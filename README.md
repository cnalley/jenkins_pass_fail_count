# README #

A quick python script to parse jenkins xml test results and save the pass and fail count as an environment variable. This is intended to be executed in a post build step in jenkins.